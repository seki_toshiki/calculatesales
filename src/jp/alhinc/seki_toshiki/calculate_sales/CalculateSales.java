package jp.alhinc.seki_toshiki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;
		//コードと支店名
		Map<String, String> codeAndName = new HashMap<>();
		//コードと売上
		Map<String,Long> codeAndSale = new HashMap<String,Long>();
		ArrayList<File> salesFiles = new ArrayList<File>();

		//エラー1,2
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		try {

			File file = new File (args[0],"branch.lst");
			//エラー3
			if(!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			 }

			 FileReader fr = new FileReader (file);
			 br = new BufferedReader (fr);

			 String line;
			 String branchCode;
			 String branchName;

			 //nullになったらループを抜ける
			 while((line = br.readLine()) != null) {
				 //支店データを","で分割
				 String[] branchSplit = line.split(",",0);
				 //エラー12,13
				 if (branchSplit.length != 2) {
					 System.out.println("支店定義ファイルのフォーマットが不正です");
					 return;
				 }

				 //エラー4~11
				 if (branchSplit[0].matches("[0-9]{3}")){
				 //一番目の要素(支店コード)
				 branchCode = branchSplit[0];
				 //二番目の要素(支店名
				 branchName = branchSplit[1];
				 //Mapに代入
				 codeAndName.put(branchCode, branchName);
				 //Mapに代入
				 codeAndSale.put(branchCode,0L);
				 } else {
					 System.out.println("支店定義ファイルのフォーマットが不正です");
					 return;
				 }
			 }
		} catch(IOException errorLog){
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException errorLog) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//ファイルを取得
		File sale = new File(args[0]);
		//ファイルの一覧を取得する
		File[] sales = sale.listFiles();
		//売上ファイルかどうかの判断
		for(int i=0; i<sales.length; ++i){
			//売上ファイルだったらリストに格納
			 String name = sales[i].getName();
			 //エラー15~18
			if(name.matches("[0-9]{8}.rcd") && sales[i].isFile()) {
				salesFiles.add(sales[i]);
			}
		}

		//連番チェック(エラー14)
		for(int i = 0; i < salesFiles.size() - 1; i++) {

			String check1 = salesFiles.get(i).getName().substring(0, 8);
			int first = Integer.parseInt(check1);

			String check2 = salesFiles.get(i + 1).getName().substring(0, 8);
			int second = Integer.parseInt(check2);

			if( second - first != 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//集計部分～～～～～～～～～～～～～～～～～～～～～～
		BufferedReader slFileBR = null;
		//salesFiles → 検索したファイルだけのArrayList
		try {
			for(int i = 0; i < salesFiles.size(); i++) {

				FileReader slFileRD = new FileReader(salesFiles.get(i));
				slFileBR = new BufferedReader(slFileRD);

				//slFileBRの中身が入ったリスト
				List<String> slFileContents = new ArrayList<String>();

				//slFileContentsの[0]にコード、[1]に売上を格納
				String str;

				while((str = slFileBR.readLine()) != null){
					slFileContents.add(str);
				}

				//エラー25,26
				if ( slFileContents.size() != 2 ) {
					System.out.println(salesFiles.get(i).getName() + "のフォーマットが不正です");
					slFileRD.close();
					return;
				}

				//エラー19~22
				if (!(slFileContents.get(1).matches("^[0-9]+$"))) {
					System.out.println("予期せぬエラーが発生しました");
					slFileRD.close();
					return;
				}

				//エラー24
				if (!(codeAndSale.containsKey(slFileContents.get(0)))) {
					System.out.println(salesFiles.get(i).getName() + "の支店コードが不正です");
					slFileRD.close();
					return;
				}

				//売上を代入＆Longに変換
				Long uriage;
				uriage = Long.parseLong(slFileContents.get(1));
				//今までの売上
				Long uriageTotal;
				uriageTotal = codeAndSale.get(slFileContents.get(0));
				//加算
				Long uriageReal = uriage + uriageTotal;
				//エラー23
				if (uriageReal > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					slFileBR.close();
					return;

				}
				//合計金額をマップにしまいなおす
				codeAndSale.put(slFileContents.get(0),uriageReal);
			}
		} catch (IOException errorLog) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if ( slFileBR != null) {
				try {
					slFileBR.close();
				} catch (IOException errorLog){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//出力


		if(!(fileOut(codeAndName,codeAndSale,"branch.out",args[0] ))) {
			return;
		}

		/*try {

			File file = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(Map.Entry<String, String> entry : codeAndName.entrySet()){

				String out;
				out = entry.getKey() + "," + entry.getValue() + "," + codeAndSale.get(entry.getKey());
				bw.write(out);
				bw.newLine();
			}
		} catch (IOException errorLog){
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if ( bw != null ) {
				try {
					bw.close();
				} catch (IOException errorLog) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}*/
	}
	public static boolean fileOut( Map<String,String> codeName, Map<String,Long> sales, String fileOutName, String pass){

		BufferedWriter bw = null;

		try {
			File fl = new File (pass,fileOutName);
			FileWriter fw = new FileWriter(fl);
			bw = new BufferedWriter(fw);

			for(Map.Entry<String, String> entry : codeName.entrySet()){

				bw.write(entry.getKey() + "," + entry.getValue() + "," + sales.get(entry.getKey()));
				bw.newLine();
			}
		} catch (IOException errorLog){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if ( bw != null ) {
				try {
					bw.close();
				} catch (IOException errorLog) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}









